const path = require('path');

const PATHS = {
    src: path.join(__dirname + '/src'),
    dist: path.join(__dirname + '/dist')
};

const HtmlWebpackPlugin = require('html-webpack-plugin');

const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
    template: path.join(PATHS.src + '/index.html'),
    filename: 'index.html',
    inject: 'body'
});

module.exports = {
    entry: [
        path.join(PATHS.src + '/app.js')
    ],
    output: {
        path: PATHS.dist,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
            { test: /\.css$/, loader: "style-loader!css-loader" }
        ]
    },
    plugins: [HTMLWebpackPluginConfig]
};
