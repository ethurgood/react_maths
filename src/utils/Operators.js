function shuffledCopy(array) {
    var shuffledArray = array.slice();
    var j, x, i;
    for (i = shuffledArray.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = shuffledArray[i - 1];
        shuffledArray[i - 1] = shuffledArray[j];
        shuffledArray[j] = x;
    }
    return shuffledArray;
}

var Operators = {
    'ADDITION': {'order': 1, 'operatorChar': '+', display: 'Addition',
        calculate: function(a, b) { return Number(a) + Number(b)},
        getOperands: function(number, numberOfQuestions) {
            let operands = [];
            let choices = Array(numberOfQuestions).fill().map((_, i) => (i+1));
            let randomChoices = shuffledCopy(choices);
            let randomChoices2 = shuffledCopy(choices);

            for(var i = 0; i < randomChoices.length; i++) {
                if(number == 'Mixed') {
                    operands.push([randomChoices[i], randomChoices2[i]]);
                } else {
                    operands.push([number, randomChoices[i]]);
                }
            }
            return operands;
        }},
    'SUBTRACTION': {'order': 2, 'operatorChar': '-', display: 'Subtraction',
        calculate: function(a, b) {return Number(a) - Number(b)},
        getOperands: function(number, numberOfQuestions) {
            let operands = [];
            let choices = Array(numberOfQuestions).fill().map((_, i) => (i+1));
            let randomChoices = shuffledCopy(choices);
            let randomChoices2 = shuffledCopy(choices);

            for(var i = 0; i < randomChoices.length; i++) {
                var operand1;
                var operand2;
                if(number == 'Mixed') {
                    operand1 = randomChoices[i];
                    operand2 = randomChoices2[i];
                } else {
                    operand1 = number;
                    operand2 = randomChoices[i];
                }
                operands.push([Math.max(operand1, operand2), Math.min(operand1, operand2)]);
            }
            return operands;
        }},
    'MULTIPLICATION': {'order': 3, 'operatorChar': '×', display: 'Multiplication',
        calculate: function(a,b) { return Number(a) * Number(b) },
        getOperands: function(number, numberOfQuestions) {
            let operands = [];
            let choices = Array(numberOfQuestions).fill().map((_, i) => (i+1));
            let randomChoices = shuffledCopy(choices);
            let randomChoices2 = shuffledCopy(choices);

            for(var i = 0; i < randomChoices.length; i++) {
                var operand1;
                var operand2;
                if(number == 'Mixed') {
                    operand1 = randomChoices[i];
                    operand2 = randomChoices2[i];
                } else {
                    operand1 = number;
                    operand2 = randomChoices[i];
                }
                operands.push([operand1, operand2]);
            }
            return operands;
        }},
    'DIVISION': {'order': 4, 'operatorChar': '÷', display: 'Division',
        calculate: function(a, b) { return Number(a) / Number(b)},
        getOperands: function(number, numberOfQuestions) {
            let operands = [];
            let choices = Array(numberOfQuestions).fill().map((_, i) => (i+1));
            let randomChoices = shuffledCopy(choices);
            let randomChoices2 = shuffledCopy(choices);

            for(var i = 0; i < randomChoices.length; i++) {
                var operand1;
                var operand2;
                if(number == 'Mixed') {
                    operand1 = randomChoices[i];
                    operand2 = randomChoices2[i];
                } else {
                    operand1 = randomChoices[i];
                    operand2 = number;
                }
                operands.push([operand1 * operand2, operand2]);
            }
            return operands;
        }}
};

export default Operators;
