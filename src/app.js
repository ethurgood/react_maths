import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import QuizParent from './containers/QuizParent'
import Redux from 'redux'
import { createStore } from 'redux'
import quizApp from './reducers'

require('./css/base.css');
require('purecss/build/pure-min.css');
require('purecss/build/grids-responsive-min.css');
require('purecss/build/buttons-min.css');

let store = createStore(quizApp);

render(
    <Provider store={store}>
        <QuizParent />
    </Provider>,
    document.getElementById('app')
);
