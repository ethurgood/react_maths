import React from 'react';
import QuizSettings from '../components/QuizSettings';
import QuizSettingsContainer from './QuizSettingsContainer';
import QuizQuestionList from './QuizQuestionList';
import Operators from '../utils/Operators';


var QuizParent = React.createClass({
    getInitialState: function() {
        return {'operator': Operators.ADDITION, 'number': 'Mixed', 'questionCount': 12};
    },
    render: function() {
        return (
            <div className="container-fluid">
                <QuizSettingsContainer />
                <QuizQuestionList />
            </div>
        );
    }
});

export default QuizParent;