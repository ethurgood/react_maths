import React from 'react';
import { connect } from 'react-redux';
import QuizQuestionGrid from '../components/QuizQuestionGrid';
import {questionBlurred, questionSubmitted, questionClicked, questionKeyUp} from '../actions';


const mapStateToProps = (state) => {
    return {
        questions: state.questions
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onQuestionBlur: (id, event) => {
            dispatch(questionBlurred(id, event));
        },
        onQuestionSubmit: (id, event) => {
            dispatch(questionSubmitted(id, event));
        },
        onQuestionKeyUp: (id, event) => {
            dispatch(questionKeyUp(id, event));
        },
        onQuestionClick: (id, event) => {
            dispatch(questionClicked(id, event));
        }
    }
};

const QuizQuestionList = connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizQuestionGrid);


export default QuizQuestionList;