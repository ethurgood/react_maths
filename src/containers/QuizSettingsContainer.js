import React from 'react';
import { connect } from 'react-redux';
import QuizSettings from '../components/QuizSettings';
import { numberChanged, operatorChanged } from '../actions';


const mapStateToProps = (state) => {
    return {
        selectedNumber: state.settings.selectedNumber,
        selectedOperator: state.settings.selectedOperator,
        numberChoices: state.settings.numberChoices
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onNumberClick: (id, event) => {
            dispatch(numberChanged(id, event));
        },
        onOperatorClick: (id, event) => {
            dispatch(operatorChanged(id, event));
        }
    }
};

const QuizSettingsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizSettings);


export default QuizSettingsContainer;