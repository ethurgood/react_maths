export const questionSubmitted = (id, event) => {
    return { type: 'QUESTION_SUBMITTED', id, event };
};

export const questionBlurred= (id, event) => {
    return { type: 'QUESTION_BLURRED', id, event };
};

export const questionClicked = (id, event) => {
    return { type: 'QUESTION_CLICKED', id, event };
};

export const questionKeyUp = (id, event) => {
    return { type: 'QUESTION_KEYUP', id, event };
};

export const numberChanged = (event, number) => {
    return { type: 'NUMBER_CHANGED', event, number };
};

export const operatorChanged = (event, operator) => {
    return { type: 'OPERATOR_CHANGED', event, operator };
};