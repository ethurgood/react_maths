import React, { PropTypes } from 'react'
import QuizQuestion from './QuizQuestion'


let QuizQuestionGrid = ({ questions, onQuestionSubmit, onQuestionBlur, onQuestionKeyUp, onQuestionClick}) => {

    return (<div className="questions pure-g">
        {questions.map((question, i) =>
            <QuizQuestion
                key={question.id}
                {... question}
                onQuestionSubmit={(e) => onQuestionSubmit(question.id, e)}
                onQuestionBlur={(e) => onQuestionBlur(question.id, e)}
                onQuestionKeyUp={(e) => onQuestionKeyUp(question.id, e)}
                onQuestionClick={(e) => onQuestionClick(question.id, e)}
            />
        )}
    </div>)
};

QuizQuestionGrid.propTypes = {
    questions: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        correct: PropTypes.bool,
        operator: PropTypes.object.isRequired,
        operand1: PropTypes.number.isRequired,
        operand2: PropTypes.number.isRequired
    })).isRequired,
    onQuestionClick: PropTypes.func.isRequired,
    onQuestionSubmit: PropTypes.func.isRequired,
    onQuestionBlur: PropTypes.func.isRequired,
    onQuestionKeyUp: PropTypes.func.isRequired
};

export default QuizQuestionGrid;
