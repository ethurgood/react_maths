import React, { PropTypes } from 'react';
import Operators from '../utils/Operators';

let QuizSettings = ({selectedOperator, selectedNumber, numberChoices, onNumberClick, onOperatorClick}) => {

    let numberOptions = [];

    for(var i = 0; i < numberChoices.length; i++) {
        let buttonClassName = "settings-button pure-button";
        let number = numberChoices[i];
        if(number == selectedNumber) {
            buttonClassName += " pure-button-primary";
        }
        numberOptions.push(
            <button key={'number' + number}
                    className={buttonClassName}
                    onClick={(e) => onNumberClick(e, number)}
            >{number}</button>)
    }

    var operators = [];
    for(var operatorKey in Operators) {
        if(Operators.hasOwnProperty(operatorKey)) {
            let operator = Operators[operatorKey];
            let buttonClassName = "settings-button pure-button";
            if(operator == selectedOperator) {
                buttonClassName += " pure-button-primary";
            }
            operators.push(
                <button key={"operator" + operatorKey}
                        className={buttonClassName}
                        onClick={(e) => onOperatorClick(e, operator)}>
                    {operator.display}</button>);
        }
    }

    return (
        <div className="pure-g">
            <div className="pure-u-1-6">
                <h3>Math Quiz</h3>
            </div>
            <div className="pure-u-5-12">
                {numberOptions}
            </div>
            <div className="pure-u-5-12">
                {operators}
            </div>
        </div>
    );
};

QuizSettings.propTypes = {
    selectedOperator: PropTypes.object.isRequired,
    selectedNumber: PropTypes.string.isRequired,
    numberChoices: PropTypes.arrayOf(PropTypes.any).isRequired,
    onNumberClick: PropTypes.func.isRequired,
    onOperatorClick: PropTypes.func.isRequired
};

export default QuizSettings;