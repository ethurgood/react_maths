import React, { PropTypes } from 'react';
import {questionAnswered, questionClicked, questionKeyUp} from '../actions';

let QuizQuestion = ({ onQuestionClick, onQuestionSubmit, onQuestionBlur, onQuestionKeyUp, operand1, operand2, operator, correct, autoFocus }) => {
    let classNames = "mathQuestion pure-u-1-8 well";
    if(correct == true) {
        classNames += " well-correct"
    }else if (correct == false) {
        classNames += " well-incorrect"
    }

    return (
        <div className={classNames} onClick={onQuestionClick}>
            <form onSubmit={onQuestionSubmit}>
                <div className="operand">{operand1}</div>
                <div className="operator">{operator.operatorChar}</div>
                <div className="operand">{operand2}</div>
                <hr className="answerSeparator" />
                <input className="suppliedAnswer"
                       type="text"
                       min="0"
                       max="999"
                       step="1"
                       maxLength="3"
                       onBlur={onQuestionBlur}
                       onKeyUp={onQuestionKeyUp}
                       autoFocus={autoFocus}
                />
            </form>
        </div>
    );
};

QuizQuestion.propTypes = {
    onQuestionClick: PropTypes.func.isRequired,
    onQuestionSubmit: PropTypes.func.isRequired,
    onQuestionBlur: PropTypes.func.isRequired,
    onQuestionKeyUp: PropTypes.func.isRequired,
    operand1: PropTypes.number.isRequired,
    operand2: PropTypes.number.isRequired,
    operator: PropTypes.object.isRequired,
    correct: PropTypes.bool,
    autoFocus: PropTypes.bool
};

export default QuizQuestion;