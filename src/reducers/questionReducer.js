function getQuestionsFromSettings(settings) {
    // Rebuild a list of questions based on the current settings
    let allOperands = settings.selectedOperator.getOperands(settings.selectedNumber, settings.numberOfQuestions);
    return allOperands.map((operands, i) => (
    {
        id: settings.selectedOperator.display + '|' + settings.selectedNumber + "|" + i,
        operand1: operands[0],
        operand2: operands[1],
        operator: settings.selectedOperator,
        offset: i,
        correct: undefined,
        //focus on the first question
        autoFocus: (i == 0),
        suppliedAnswer: ""
    }
    ));
}

const questionsReducer = (state, action, settings) => {
    if(state === undefined) {
        return getQuestionsFromSettings(settings);
    }
    switch(action.type) {
        case 'OPERATOR_CHANGED':
        case 'NUMBER_CHANGED':
            return getQuestionsFromSettings(settings);
        case 'QUESTION_SUBMITTED':
        case 'QUESTION_BLURRED':
        case 'QUESTION_KEYUP':
        case 'QUESTION_CLICKED':
            return state.map(q => question(q, action));
        default:
            return state;
   }
};

const question = (state, action) => {
    if(state.id !== action.id) {
        return state;
    }

    var correctAnswer;

    switch(action.type) {
        case 'QUESTION_CLICKED':
            return Object.assign({}, state, {autoFocus: true});
        case 'QUESTION_SUBMITTED':
            if(state.suppliedAnswer == "" || state.suppliedAnswer === undefined) {
                return state;
            }
            action.event.preventDefault();
            correctAnswer = state.operator.calculate(state.operand1, state.operand2);
            return Object.assign({}, state, {
                correct: state.suppliedAnswer == correctAnswer
            });
        case 'QUESTION_BLURRED':
            if(state.suppliedAnswer == "" || state.suppliedAnswer === undefined) {
                return state;
            }
            correctAnswer = state.operator.calculate(state.operand1, state.operand2);
            return Object.assign({}, state, {
                correct: state.suppliedAnswer == correctAnswer
            });
        case 'QUESTION_KEYUP':
            if (action.event.keyCode == 27) {
                action.event.target.value = "";
            }

            // If the answer was cleared, reset the correct state
            if (action.event.target.value == "") {
                return Object.assign({}, state, {
                    correct: undefined,
                    suppliedAnswer: ""
                });
            }
            return Object.assign({}, state, {
                suppliedAnswer: action.event.target.value}
            );
        default:
            return state;
    }
};

export default questionsReducer;