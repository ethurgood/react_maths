import { combineReducers } from 'redux';
import settingsReducer from './settingsReducer'
import questionsReducer from './questionReducer'

const quizApp = (state={}, action) => {
    // Note: combineReducers can't be used here because it completely isolates
    // the sections of the state, but questionsReducer needs to know the
    // state of the settings
    let settings = settingsReducer(state.settings, action);
    let questions = questionsReducer(state.questions, action, settings);
    return {settings, questions}
};

export default quizApp;