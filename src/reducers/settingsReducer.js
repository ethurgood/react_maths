import Operators from '../utils/Operators'

const _DEFAULT_NUMBER_OF_QUESTONS = 12;

const settingsReducer = (state, action) => {
    if(state === undefined) {
        let baseState = {numberOfQuestions: _DEFAULT_NUMBER_OF_QUESTONS,
                selectedNumber: 'Mixed',
                selectedOperator: Operators.ADDITION,
                numberChoices: Array(12).fill().map((_,i) => i+1).concat(['Mixed'])};
        console.log(baseState);
        return baseState;
    }

    switch(action.type) {
        case 'NUMBER_CHANGED':
            return Object.assign({}, state, {selectedNumber: action.number});
        case 'OPERATOR_CHANGED':
            return Object.assign({}, state, {selectedOperator: action.operator});
        case 'NUMBER_OF_QUESTIONS_CHANGED':
        default:
            return state;
    }
};

export default settingsReducer;